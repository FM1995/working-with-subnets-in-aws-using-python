import boto3

# Establish connection to the EC2 service
ec2_client = boto3.client('ec2', region_name="eu-west-2")

# Retrieve subnets in the specified region
subnets = ec2_client.describe_subnets()

# Print SubnetIds of all subnets
for subnet in subnets['Subnets']:
    subnet_id = subnet['SubnetId']
    print(subnet_id)



