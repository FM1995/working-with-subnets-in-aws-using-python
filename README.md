# Working with Subnets in AWS using Python

#### Project Outline

In this project we will get all the subnets in our default region and print out the subnet id's

#### Lets get started

First we need to create a new project and install boto3

```
pip install boto3
```

![Image1](https://gitlab.com/FM1995/working-with-subnets-in-aws-using-python/-/raw/main/Images/Image1.png)

Now using the below documentation we can get the subnets in my default region

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_subnets.html

First let’s ensure the default region and access keys are configured and ready to be connected

![Image2](https://gitlab.com/FM1995/working-with-subnets-in-aws-using-python/-/raw/main/Images/Image2.png)

Can start of with the below code and get all the unarranged subnet id's

![Image3](https://gitlab.com/FM1995/working-with-subnets-in-aws-using-python/-/raw/main/Images/Image3.png)

Finalised code with results

![Image4](https://gitlab.com/FM1995/working-with-subnets-in-aws-using-python/-/raw/main/Images/Image4.png)

Can also cross check in the console aswell

![Image5](https://gitlab.com/FM1995/working-with-subnets-in-aws-using-python/-/raw/main/Images/Image5.png)


